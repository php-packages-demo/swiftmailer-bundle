# [swiftmailer-bundle](https://phppackages.org/p/symfony/swiftmailer-bundle)

[**symfony/swiftmailer-bundle**](https://packagist.org/packages/symfony/swiftmailer-bundle) [295](https://phppackages.org/s/swiftmailer-bundle) Symfony Swiftmailer Bundle https://symfony.com/swiftmailer-bundle

* As of November 2021, Swiftmailer will not be maintained anymore. Use [Symfony Mailer](https://symfony.com/doc/current/mailer.html) instead. Read more on [Symfony's blog](https://symfony.com/doc/current/mailer.html).
  * A bundle may no more be needed.
  * [*Sending Emails with Mailer*
    ](https://symfony.com/doc/current/mailer.html)
